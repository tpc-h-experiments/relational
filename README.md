# Dataset Generation

1. Execute ``./dbgen -s <scale factor>`` inside ``/path/to/TPC\_H/dbgen/``.
2. Remove the extra ``|`` at the end of each line from each ``.tbl`` file produced 
   in the previous step. Example (bash):
   
   ```
   for i in`ls *.tbl`; do sed -i's/|$//'$i; done
   ```

3. Import the data into PostgreSQL using the `create_rdb.sql` script inside the `creation` folder.

	- **IMPORTANT:** The script removes the database `tpch` if it already exists and it must be modified to match
		the DBGEN path in one's filesystem.

# Useful links

- CTE as optimization fence: https://medium.com/@hakibenita/be-careful-with-cte-in-postgresql-fca5e24d2119
- Graphical visualization of PosgreSQL explain: https://explain.dalibo.com/
