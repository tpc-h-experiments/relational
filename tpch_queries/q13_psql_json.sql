\c tpch
explain analyze select
	c_count,
	count(*) as custdist
from (
  select
    custkey,
    count(*) filter (where j.c_orders->'o_comment' is not null)
  from
     (select 
            cast(j.customers->>'c_custkey' as integer) as custkey,
            jsonb_array_elements(
            case 
              when jsonb_array_length(j.customers->'c_orders') >= 1 then
     	      j.customers->'c_orders'
              else
     	      jsonb_build_array(null)
            end) as c_orders
     from scale1.json_data j
     group by custkey, c_orders) j
  where 
        j.c_orders->'o_orderkey' is null
        or j.c_orders->>'o_comment' not like '%express%packages%'
   group by
    custkey
) as c_orders (c_custkey, c_count)
group by
	c_count
order by
	custdist desc,
	c_count desc;