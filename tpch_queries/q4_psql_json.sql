\c tpch
explain analyze select
       orders.o->>'o_orderpriority' as orderpriority,
       count(*) as order_count
from
	(select jsonb_array_elements(j.customers->'c_orders') o
	from scale1.json_data j) orders
where
	cast(orders.o->>'o_orderdate' as date) >= date '1992-01-01'
	and cast(orders.o->>'o_orderdate' as date) < date '1992-01-01' + interval '3' month
	and exists (
	    select 1
	    from jsonb_array_elements(orders.o->'o_lineitems') l
	    where 
	    	  cast(l->>'l_commitdate' as date) < cast(l->>'l_receiptdate' as date)
	)
group by
      orderpriority
order by
      orderpriority;