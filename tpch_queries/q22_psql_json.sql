\c tpch
explain analyze select
	cntrycode,
	count(*) as numcust,
	sum(c_acctbal) as totacctbal
from
	(
		select
			substring(j.customers->>'c_phone' from 1 for 2) as cntrycode,
			cast(j.customers->>'c_acctbal' as decimal) as c_acctbal
		from
			scale1.json_data j
		where
			substring(j.customers->>'c_phone' from 1 for 2) in
				('30', '17', '25', '10', '22', '15', '21')
			and cast(j.customers->>'c_acctbal' as decimal) > (
				select
					avg(cast(j.customers->>'c_acctbal' as decimal))
				from
					scale1.json_data j
				where
					cast(j.customers->>'c_acctbal' as decimal) > 0.00
					and substring(j.customers->>'c_phone' from 1 for 2) in
						('30', '17', '25', '10', '22', '15', '21')
			)
			and jsonb_array_length(j.customers->'c_orders') < 1
	) as custsale
group by
	cntrycode
order by
	cntrycode;