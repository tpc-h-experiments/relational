-- Although it works, it is a very inefficient solution in terms of memory. Observe
-- how lineitems is being included twice; first inside the unnested orders and second
-- when unnesting lineitems itself
\c tpch

explain analyze select
	ol.o->>'o_orderkey' as orderkey,
       	sum(cast(ol.l->>'l_extendedprice' as decimal) * (1 - cast(ol.l->>'l_discount' as decimal))) as revenue,
       	ol.o->>'o_orderdate' as orderdate,
	ol.o->>'o_shippriority' as shippriority
from
	(select
	       jsonb_array_elements(j.customers->'c_orders') o,
	       jsonb_array_elements(jsonb_array_elements(j.customers->'c_orders')->'o_lineitems') l
	from scale1.json_data j
	where
	       j.customers->>'c_mktsegment' = 'AUTOMOBILE') ol
where
	cast(ol.o->>'o_orderdate' as date) < date '1992-01-02'
	and cast(ol.l->>'l_shipdate' as date) > date '1992-01-02'
group by
      orderkey,
      orderdate,
      shippriority
order by
      revenue desc,
      orderdate
limit 10;